'use strict';

var Task = require('./Task');

log("Using the Constructor Pattern");

var task1 = new Task("Create demo 1");
var task2 = new Task("Create demo 2");
var task3 = new Task("Create demo 3");
var task4 = new Task("Create demo 4");

task1.complete();
task2.save();
task3.save();
task4.save();

function log(input) {
  console.log(input);
}

function logn(input) {
  console.log("\n" + input);
}