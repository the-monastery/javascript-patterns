var app = module.exports = require("koa")();

app.use(function *(){
  this.body = "Hello World!";
});

var port = process.env.PORT || (process.argv[2] || 3000);
port = (typeof port != "number") ? port : 3000;

if(!module.parent){ app.listen(port); }

console.log("Application Started on port " + port);